<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
   <h1>Buat Account Baru!</h1> 
   <h3>Sign Up Form</h3>
   <form action="/welcome" method="POST">
       @csrf
       <!-- penginputan nama -->
         <label for="fname">First Name</label>
         <input type="text" name="fname">

    <br>
    <br>
         <label for="lname">Last Name</label>
         <input type="text" name="lname">
    
    <br>
    <br>
         <!-- penginputan gender -->
         <label>Gender:</label><br>
           <input type="radio" id="female" name="gender" value="female">
           <label for="female">Female</label><br>
           <input type="radio" id="male" name="gender" value="male">
           <label for="male">Male</label><br>
           <input type="radio" id="other" name="gender" value="other">
           <label for="other">Other</label>
   <br>
   <br>

   <!-- penginputan nationality -->
        <label>Nationality</label><br>
        <select name="nationality" id="nationality">
          <option value="indonesia">Indonesia</option>
          <option value="belanda">Belanda</option>
          <option value="german">German</option>
          <option value="malaysia">Malaysia</option>
        </select>
    <br>
    <br>

    <!--penginputan yang bisa-->
        <label>Language Spoken</label><br>
        <input type="checkbox" id="bahasa_indonesia" name="bahasa_indonesia" value="bahasa_indonesia">
        <label for="bahasa_indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="bahasa_inggris" name="bahasa_inggris" value="bahasa_inggris">
        <label for="bahasa_inggris">Bahasa Inggris</label><br>
        <input type="checkbox" id="bahasa_mandarin" name="bahasa_mandarin" value="bahasa_mandarin">
        <label for="bahasa_mandarin">Bahasa Mandarin</label><br>


    <br>
    <br>
        <label for="bio">Bio &nbsp;&nbsp;</label>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>

        <br>
        <input type="submit" value="Kirim Data">
    </form>         
   
</body>
</html>