<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function welcome(Request $request){
        // dd($request->all());

        $fname = $request['fname'];
        $lname = $request['lname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $bahasa_indonesia = $request['bahasa_indonesia'];
        $bio = $request['bio'];

        return view('halaman.welcome', compact('fname','lname','gender','nationality','bahasa_indonesia','bio'));
    }
}
